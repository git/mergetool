git-init(1)
===========

NAME
----
git-init - Create an empty git repository or reinitialize an existing one


SYNOPSIS
--------
'git-init' [-q | --quiet] [--template=<template_directory>] [--shared[=<permissions>]]


OPTIONS
-------

--

-q, \--quiet::

Only print error and warning messages, all other output will be suppressed.

--template=<template_directory>::

Provide the directory from which templates will be used.  The default template
directory is `/usr/share/git-core/templates`.

When specified, `<template_directory>` is used as the source of the template
files rather than the default.  The template files include some directory
structure, some suggested "exclude patterns", and copies of non-executing
"hook" files.  The suggested patterns and hook files are all modifiable and
extensible.

--shared[={false|true|umask|group|all|world|everybody}]::

Specify that the git repository is to be shared amongst several users.  This
allows users belonging to the same group to push into that
repository.  When specified, the config variable "core.sharedRepository" is
set so that files and directories under `$GIT_DIR` are created with the
requested permissions.  When not specified, git will use permissions reported
by umask(2).

The option can have the following values, defaulting to 'group' if no value
is given:

 - 'umask' (or 'false'): Use permissions reported by umask(2). The default,
   when `--shared` is not specified.

 - 'group' (or 'true'): Make the repository group-writable, (and g+sx, since
   the git group may be not the primary group of all users).

 - 'all' (or 'world' or 'everybody'): Same as 'group', but make the repository
   readable by all users.

By default, the configuration flag receive.denyNonFastforward is enabled
in shared repositories, so that you cannot force a non fast-forwarding push
into it.

--


DESCRIPTION
-----------
This command creates an empty git repository - basically a `.git` directory
with subdirectories for `objects`, `refs/heads`, `refs/tags`, and
template files.
An initial `HEAD` file that references the HEAD of the master branch
is also created.

If the `$GIT_DIR` environment variable is set then it specifies a path
to use instead of `./.git` for the base of the repository.

If the object storage directory is specified via the `$GIT_OBJECT_DIRECTORY`
environment variable then the sha1 directories are created underneath -
otherwise the default `$GIT_DIR/objects` directory is used.

Running `git-init` in an existing repository is safe. It will not overwrite
things that are already there. The primary reason for rerunning `git-init`
is to pick up newly added templates.

Note that `git-init` is the same as `git-init-db`.  The command
was primarily meant to initialize the object database, but over
time it has become responsible for setting up the other aspects
of the repository, such as installing the default hooks and
setting the configuration variables.  The old name is retained
for backward compatibility reasons.


EXAMPLES
--------

Start a new git repository for an existing code base::
+
----------------
$ cd /path/to/my/codebase
$ git-init      <1>
$ git-add .     <2>
----------------
+
<1> prepare /path/to/my/codebase/.git directory
<2> add all existing file to the index


Author
------
Written by Linus Torvalds <torvalds@osdl.org>

Documentation
--------------
Documentation by David Greaves, Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
